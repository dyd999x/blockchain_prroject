const assert = require('assert');

const ganache = require('ganache-cli');
const Web3 = require('web3');
const web3 = new Web3(ganache.provider());

const compiledInterface = require('../ethereum/build/CarRideSharing.json');

let accounts;
let interfaceContract;

beforeEach(async () => {
    accounts = await web3.eth.getAccounts();

    interfaceContract = await new web3.eth.Contract(JSON.parse(compiledInterface.interface))
        .deploy({data: compiledInterface.bytecode})
        .send({ from: accounts[0], gas:'3000000'});
});

describe('CarRideSharing Contract', ()=>{
    it('deploys the contract', ()=>{
        assert.ok(interfaceContract.options.address);
    });

    it('registers a driver', async ()=>{
        await interfaceContract.methods.registerAsDriver('Renault Symbol','Diana Ion','B 02 BNM','0745 874 687')
            .send({
                from: accounts[0],
                gas:'3000000'
            });
        const dr = await interfaceContract.methods.drivers(accounts[0]).call();
        assert.equal(dr.addr, accounts[0]);
    });

    it('adds a new ride', async ()=>{
        await interfaceContract.methods.registerAsDriver('Renault Symbol','Diana Ion','B 02 BNM','0745 874 687')
            .send({
                from: accounts[0],
                gas:'3000000'
         });
        await interfaceContract.methods.addRide('Bucuresti Sector 6','Buzau Micro 3','10.04.2020',3)
            .send({
                from: accounts[0],
                gas:'3000000'
            })
        const ride = await interfaceContract.methods.rides(0).call();
        assert.equal(0,ride.id);
        assert.equal(ride.driver,accounts[0]);
    })

    it('select ride', async ()=>{
        
        await interfaceContract.methods.registerAsDriver('Renault Symbol','Diana Ion','B 02 BNM','0745 874 687')
            .send({
                from: accounts[0],
                gas:'3000000'
        });
        await interfaceContract.methods.addRide('Bucuresti Sector 6','Buzau Micro 3','10.04.2020',3)
            .send({
                from: accounts[0],
                gas:'3000000'
            });
        const rideBefore = await interfaceContract.methods.rides(0).call();
        await interfaceContract.methods.selectRide(rideBefore.id,2,'Malin Andrei','0742 154 547')
            .send({
                from: accounts[1],
                gas: '3000000'
        });
        const rideAfter = await interfaceContract.methods.rides(0).call();
        const passenger = await interfaceContract.methods.pass(accounts[1]).call();
        assert.equal(passenger.addr,accounts[1]);
        assert.equal(rideAfter.seatsAvailable,1)
    });

    it('ride completed', async ()=>{
        await interfaceContract.methods.registerAsDriver('Renault Symbol','Diana Ion','B 02 BNM','0745 874 687')
            .send({
                from: accounts[0],
                gas:'3000000'
        });
        await interfaceContract.methods.addRide('Bucuresti Sector 6','Buzau Micro 3','10.04.2020',3)
            .send({
                from: accounts[0],
                gas:'3000000'
            });
        const rideBefore = await interfaceContract.methods.rides(0).call();
        await interfaceContract.methods.selectRide(rideBefore.id,2,'Malin Andrei','0742 154 547')
            .send({
                from: accounts[1],
                gas: '3000000'
        });
        await interfaceContract.methods.rideCompleted(rideBefore.id)
            .send({
                from: accounts[0],
                gas:'3000000'
        });
        const rideAfter = await interfaceContract.methods.rides(0).call();
        assert(rideAfter.completed);

    });

    it('rate ride', async ()=>{
        await interfaceContract.methods.registerAsDriver('Renault Symbol','Diana Ion','B 02 BNM','0745 874 687')
            .send({
                from: accounts[0],
                gas:'3000000'
        });
        await interfaceContract.methods.addRide('Bucuresti Sector 6','Buzau Micro 3','10.04.2020',3)
            .send({
                from: accounts[0],
                gas:'3000000'
            });
        const ride = await interfaceContract.methods.rides(0).call();
        await interfaceContract.methods.selectRide(ride.id,2,'Malin Andrei','0742 154 547')
            .send({
                from: accounts[1],
                gas: '3000000'
        });
        await interfaceContract.methods.rideCompleted(ride.id)
            .send({
                from: accounts[0],
                gas:'3000000'
        });
        await interfaceContract.methods.rateRide(4,ride.id)
            .send({
                from: accounts[1],
                gas: '3000000'
        });
        const driver = await interfaceContract.methods.drivers(accounts[0]).call();
        const driverRating = await interfaceContract.methods.driverRating(driver.addr).call();
        assert.equal(driverRating.veryGood,1);

    });



});
import React,{Component} from 'react';
import Layout from '../components/Layout';
import RideForm from '../components/RideForm';
import {Link} from '../routes';
import {Button,Container} from 'semantic-ui-react';
import DriverDetailsAddRide from '../components/DriverDetailsAddRide';
import web3 from '../ethereum/web3';
import carRide from '../ethereum/carRide';

class AddRide extends Component{

  
    state={
        profile:false,
        addr:'',
        name:'',
        carBrand:'',
        licenceNr:'',
        telNr:'',
        nrOfRides:'',
        driver:''
    }

    async componentWillMount(){
        const accounts = await web3.eth.getAccounts();
        const driver = await carRide.methods.drivers(accounts[0]).call();
        console.log(accounts[0]);
        console.log(driver);
        this.setState({
            driver:driver,
            addr:driver.addr,
            name:driver.name,
            carBrand:driver.carBrand,
            licenceNr:driver.licenseNr,
            telNr:driver.telNr,
            nrOfRides:driver.nrOfRides
            });
    }
    onClickProfile = async()=>{

        if(!this.state.profile){
            this.setState({profile:true});
        }else{
            this.setState({profile:false});
        }
            
        
        
    }
    

    render(){
        return(
            <Layout>
                <h3 style={{marginTop:'1rem',marginBottom:'1rem'}}>Now you can share your rides!</h3>
                <Button 
                            floated='right' 
                            basic 
                            color='purple' 
                            style={{marginBottom:'0.5rem'}}
                            onClick={this.onClickProfile}
                            >
                            My Driver Profile
                </Button>
                <Link route='/'>
                    <a>
                        Back
                    </a>
                </Link>
                <Container fluid>
                <DriverDetailsAddRide 
                            show={this.state.profile}
                            addr={this.state.addr}
                            name={this.state.name} 
                            telNr={this.state.telNr}
                            carBrand={this.state.carBrand}
                            licenceNr={this.state.licenceNr}
                            nrOfRides={this.state.nrOfRides}
                />
               </Container >
               <RideForm />
            </Layout>
        )
    }
}

export default AddRide;
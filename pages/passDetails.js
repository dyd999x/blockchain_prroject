import React,{Component} from 'react';
import Layout from '../components/Layout';
import PassDetails from '../components/PassDetails';
import carRide from '../ethereum/carRide';
import {Card,Segment,Icon,Header} from 'semantic-ui-react';

class passDetails extends Component{

    static async getInitialProps(props){
        const {id} = props.query;
        const pass = await carRide.methods.getRidePassengers(id).call();
        const pDetails = await Promise.all(
            Array(parseInt(pass.length))
                .fill()
                .map((el,index)=>{return carRide.methods.pass(pass[index]).call();})
        );
        return {pDetails};
    }

    renderCards(){
        return this.props.pDetails.map((pass,index)=>{
            return(
                <PassDetails
                    pass={pass}
                />
            );
        });
    }

    render(){
        if(this.props.pDetails.length >0){
            return(
                <Layout>
                    <Card.Group>
                        {this.renderCards()}
                    </Card.Group>
                   
                </Layout>
            )
        }else{
            return(
                <Layout>
                    <Segment placeholder>
                        <Header icon>
                            <Icon name='address card' />
                            No passengers info to show for this ride
                        </Header>
                    </Segment>
                </Layout>
            )
            
            
        }
        
    }

}

export default passDetails;
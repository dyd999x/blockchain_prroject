import React,{Component} from 'react';
import Layout from '../components/Layout';
import SelectRide from '../components/SelectRide';
import {Link} from '../routes';

class selectRide extends Component{

    static async getInitialProps(props){
        const {account,id,seatsAv} = props.query;
        return {account,id,seatsAv};
    }

    render(){
        return(
            <Layout>
                <Link route='/'>
                    <a>
                        Back
                    </a>
                </Link>
                <SelectRide 
                     account={this.props.account} 
                     rideID={this.props.id}
                     seatsAv={this.props.seatsAv}/>
            </Layout>
        )
    }

}

export default selectRide;
import React,{Component} from 'react';
import web3 from '../ethereum/web3';
import carRide from '../ethereum/carRide';
import Layout from '../components/Layout';
import Rides from '../components/Rides';
import {Button,Container} from 'semantic-ui-react';
import {Link,Router} from '../routes';
import DriverCard from '../components/DriverCard';
import RateDriver from '../components/RateDriver';
import _ from 'lodash';
import SearchBar from '../components/SearchBar';

class AppIndex extends Component{
    state={
        driver:false,
        profile:false,
        account:'',
        addr:'',
        name:'',
        carBrand:'',
        licenceNr:'',
        telNr:'',
        nrOfRides:'',
        rating:'',
        nrOfVotes:'',
        showRate:false,
        filteredRides:[]
    };
    static async getInitialProps(){
       
        const nrOfRides = await carRide.methods.rideID().call();
        let rides = await Promise.all(
            Array(parseInt(nrOfRides))
                .fill()
                .map((el,index)=>{return carRide.methods.rides(index).call();})
        );
        rides=rides.reverse();
        let driversAddrs=[];
        for(var i=0;i<rides.length;i++){
            driversAddrs.push(rides[i].driver);
        }
        const ratings = await Promise.all(
            Array(parseInt(driversAddrs.length))
                .fill()
                .map((el,index)=>{return carRide.methods.driverRating(driversAddrs[index]).call();})
        );
        let driversRatings=[];
        for(var k=0;k<ratings.length;k++){
            let j=5;
            let rating =0;
            let nr=0;
            for(var i=0;i<5;i++){
                rating+=parseInt(ratings[k][i])*j;
                nr+=parseInt(ratings[k][i]);
                j--;
            }
            if(nr!=0)rating /= nr;
            else rating=0;
            driversRatings.push(rating);
        }
        
        const source= rides.map((el,index)=> {return ({
            id:el.id,
            from:el.startLocation,
            to:el.destination    
        }) });

        const sourceSearch= source;
     
        return {nrOfRides,rides,driversRatings,sourceSearch};
    }

    async componentWillMount(){
     
        this.setState({showRate:false});
        const accounts = await web3.eth.getAccounts();
        this.setState({account:accounts[0]});
        const dr = await carRide.methods.drivers(accounts[0]).call();
        if(dr.name !== '')this.setState({driver:true});
        this.setState({profile:false});

           
        this.setState({
            filteredRides : this.props.rides
        })

    }


    onClickProfile = async()=>{

        if(!this.state.profile){
            const accounts = await web3.eth.getAccounts();
            const driver = await carRide.methods.drivers(accounts[0]).call();
            const ratings = await carRide.methods.driverRating(accounts[0]).call();
            let j=5;
            let rating =0;
            let nr=0;
            for(var i=0;i<5;i++){
                rating+=parseInt(ratings[i])*j;
                nr+=parseInt(ratings[i]);
                j--;
            }
            rating /= nr;
            this.setState({profile:true});
            this.setState({
            addr:driver.addr,
            name:driver.name,
            carBrand:driver.carBrand,
            licenceNr:driver.licenseNr,
            telNr:driver.telNr,
            nrOfRides: driver.nrOfRides,
            rating:rating,
            nrOfVotes:nr
        });
        }else{
            this.setState({profile:false})
        }
        
    }
    onSearchTextChange = (value) => {
        if (!value) return
        this.setState({
            filteredRides : this.props.rides.filter(
                r => r.startLocation.toLowerCase().indexOf(value.toLowerCase())!= -1
                 ||  r.destination.toLowerCase().indexOf(value.toLowerCase())!= -1
            )
        })
    }
    onClickRate = async()=>{
        if(!this.state.showRate){
            this.setState({showRate:true});
        }else{
            this.setState({showRate:false});
        }
    }

    render(){

        return(
           <Layout>
                <Container fluid>
                    <h3>Search a location</h3>
                    <SearchBar  onSearch={this.onSearchTextChange} sourceSearch={this.props.sourceSearch} />
                </Container>
               <Container >
               <Button.Group floated='right'>
                    <Button 
                            disabled={!this.state.driver} 
                            floated='right' 
                            basic 
                            color='purple' 
                            style={{marginBottom:'0.5rem'}}
                            onClick={this.onClickProfile}
                            >
                            My Driver Profile
                    </Button>
                    <Button 
                        disabled={!this.state.driver}
                        style={{marginBottom:'0.5rem'}}
                        basic 
                        color='purple' 
                        content='Add Ride' 
                        onClick={()=>Router.pushRoute('/addRide')}
                        />
                    <Link route='/registerDriver'>
                            <Button  disabled={this.state.driver} floated='right' basic color='purple' style={{marginBottom:'0.5rem'}}>
                                    Register as Driver
                            </Button>
                    </Link>

                    <Button 
                            floated='right'
                            basic
                            color='teal'
                            style={{marginBottom:'0.5rem'}}
                            onClick={this.onClickRate}
                            >
                            Rate Ride
                    </Button>
                    
                   
               </Button.Group>
    
               </Container>
                
               <Container fluid>
                <DriverCard 
                        rides={this.props.rides}
                        show={this.state.profile}
                        addr={this.state.addr}
                        name={this.state.name}
                        carBrand={this.state.carBrand}
                        licenceNr={this.state.licenceNr}
                        telNr={this.state.telNr}
                        nrOfRides={this.state.nrOfRides}
                        rating={this.state.rating}
                        nrOfVotes={this.state.nrOfVotes}
                />
               </Container >
               <Container fluid>
                    <RateDriver account={this.state.account} show={this.state.showRate} />
                </Container>
               
               <Rides 
                    rides={this.state.filteredRides}
                    driversRatings={this.props.driversRatings}
                    account={this.state.account}    
                />
                
               
           </Layout>
        )
    }
}

export default AppIndex;
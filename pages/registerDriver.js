import React,{Component} from 'react';
import Layout from '../components/Layout';
import DriverForm from '../components/DriverForm';
import {Link} from '../routes';

class registerDriver extends Component{
    render(){
        return(
            <Layout>
                <h3 style={{marginTop:'1rem',marginBottom:'1rem'}}>Register as driver and share your rides!</h3>
                <Link route='/'>
                    <a>
                        Back
                    </a>
                </Link>
                <DriverForm />
            </Layout>
        )
    }
}

export default registerDriver;
# Blockchain_prroject

# Ride Sharing DApp!

## Description

This decentralized application allows its user to share car rides. Anyone can register as driver and input the details of a ride he/she wants to share with someone else. People that are looking for rides can search for a suitable one based on the location. After a ride is marked as completed by the driver, the passengers can rate it. A driver with a good rating is more likely to be chosen in the future


## Functionality



On the first page all the rides are listed. There is a seach bar where users can search rides by destination or starting location. Then, by clicking **Select Ride** a new page will appear with the ride details, driver details and a form for personal information from the passenger .

The **Register as driver** option is enabled only for non-driver accounts and, after a person registers, he/she will be redirected to the form for adding a new ride.

Depending on the account address of your Metamask, if you have not registered as driver previously, the options **Add Ride** and **My driver profile** will be disabled and only **Register as driver** can be selected. 

The **Rate Ride** option will work only if the ride ID is correct and you have been a passenger of that ride.

The **forms** have to be completed as the placeholder indicate.


## Observations



The account is deployed at the address visible in /ethereum/carRide.js file.
The deploy.js file from the ethereum folder can be modified so that the contract can be deployed again. The MetaMask seed words and an Infura API key are required.


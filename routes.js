const routes = require('next-routes')();

routes
    .add('/registerDriver','/registerDriver')
    .add('/addRide','/addRide')
    .add('/selectRide/:account/:id/:seatsAv','/selectRide')
    .add('/passDetails/:id','/passDetails');
module.exports = routes;
import _ from 'lodash';
import PropTypes from 'prop-types';
import React, { Component } from 'react'
import { Search, Grid, Header, Segment,Container,Label} from 'semantic-ui-react'


const resultRenderer = ({ from,to }) => <Label content={`${from} to ${to}`} />

resultRenderer.propTypes = {
  from: PropTypes.string,
  to: PropTypes.string,
  onSearch:PropTypes.func
}


export default class SearchBar extends Component {


   async componentWillMount() {
    
    const source = this.props.sourceSearch;
    this.setState({source: source});
    this.resetComponent();
    
  }

  resetComponent = () =>
    this.setState({ isLoading: false, results: [], value: '' })

  handleResultSelect = (e, { result }) => {
    this.props.onSearch(result.from);
    this.setState({ value: result.from })
  }

  handleSearchChange = (e, { value }) => {
    this.setState({ isLoading: true, value })
    this.props.onSearch(value);
    setTimeout(() => {
      if (this.state.value.length < 1) return this.resetComponent()

      const re = new RegExp(_.escapeRegExp(this.state.value), 'i')
      const isMatch = result => re.test(result.from)

      this.setState({
        isLoading: false,
        results: _.filter(this.state.source, isMatch)
      })
    }, 300)
  }

  render() {
    const { isLoading, value, results } = this.state
    return (
        <Container fluid >
            <Grid>
            <Grid.Column width={6}>
            <Search
            loading={isLoading}
            onResultSelect={this.handleResultSelect}
            onSearchChange={_.debounce(this.handleSearchChange, 500, {
              leading: true,
            })}
            results={results}
            value={value}
            resultRenderer={resultRenderer}
            {...this.props}
          />
            </Grid.Column>
        </Grid>
        </Container>
      
    )
  }
}

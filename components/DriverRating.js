import React from 'react';
import {Rating} from 'semantic-ui-react';


const DriverRating =(props) =>{
    return <div>
        <Rating  size={props.size} floated='right' disabled icon='star' rating={props.rating} maxRating={5}/> 
    </div>
}

export default DriverRating;
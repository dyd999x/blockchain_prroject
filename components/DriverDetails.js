import React,{Component} from 'react';
import {Card,Button} from 'semantic-ui-react'
import DriverRating from './DriverRating';

class DriverDetails extends Component{

    render(){
        const size='small';
        return(
            <div>
                    <Card fluid  color='violet'>
                    <Card.Content>
                        <Card.Header>{this.props.name}</Card.Header>
                        <Card.Description>
                            <h5>Telephone Number:</h5> {this.props.telNr}
                            <hr />
                            <h5>Car Brand:</h5> {this.props.carBrand}
                            <hr />
                            <h5>Licence Number:</h5> {this.props.licenceNr}
                            <hr />
                            <h5>Number of Rides:</h5> {this.props.nrOfRides}
                            <hr />
                            <h5>Rating:</h5>
                            <h6>{this.props.nrOfVotes} Votes</h6>
                            <DriverRating rating={this.props.rating} size={size}/>
                        </Card.Description>
                    </Card.Content>
                    </Card>
                    </div>
        );
        
    }
}

export default DriverDetails;


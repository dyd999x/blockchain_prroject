import React,{Component} from 'react';
import {Table} from 'semantic-ui-react';
import carRide from '../ethereum/carRide';

class RideSelected extends Component{

    state={
        ride:''
    }

    async componentWillMount(){
        const ride = await carRide.methods.rides(this.props.rideID).call();
        this.setState({ride:ride});
    }
    render(){
        const {Header,Row,HeaderCell,Body,Cell} = Table;
        return(
            <Table striped color="violet">
                <Header>
                    <Row textAlign='center'>
                        <HeaderCell >Ride ID</HeaderCell>
                        <HeaderCell >From</HeaderCell>
                        <HeaderCell>Destination</HeaderCell>
                        <HeaderCell >Seats Available</HeaderCell>
                        <HeaderCell>Date</HeaderCell>
                    </Row>
                </Header>
                <Body>
                    <Row textAlign='center' disabled={this.state.ride.completed} >
                        <Cell >{this.props.rideID}</Cell>
                        <Cell >{this.state.ride.startLocation}</Cell>
                        <Cell >{this.state.ride.destination}</Cell>
                        <Cell >{this.state.ride.seatsAvailable}</Cell>
                        <Cell>{this.state.ride.rideDate}</Cell>
                    </Row>
                </Body>
            </Table>
        )
    }
}
export default RideSelected;
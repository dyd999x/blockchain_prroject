import React,{Component} from 'react';
import {Card,Button,Segment,Table} from 'semantic-ui-react';
import DriverRideRow from './DriverRideRow';

class DriverRides extends Component{

    renderRows(){
        if(this.props.rides.length == 0)return null;
        return this.props.rides.map((ride,index)=>{
            return(
                <DriverRideRow
                    key={index}
                    id={ride.id}
                    ride={ride}
                    addr={this.props.addr}
                />
            );
        });
    }
    render(){
        if(this.props.show){
            const {Header,Row,HeaderCell,Body} = Table;
            return(
            <Table striped color="red">
                <Header>
                    <Row textAlign='center'>
                        <HeaderCell>Ride ID</HeaderCell>
                        <HeaderCell>From</HeaderCell>
                        <HeaderCell>Destination</HeaderCell>
                        <HeaderCell>Date</HeaderCell>
                        <HeaderCell>Status</HeaderCell>
                        <HeaderCell>Passengers</HeaderCell>
                    </Row>
                </Header>
                <Body>
                    {this.renderRows()}
                </Body>
            </Table>
        )
        }else{
            return <div></div>
        }
    }
}

export default DriverRides;


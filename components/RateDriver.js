import React, { Component } from 'react'
import { Rating,Button,Input,Message} from 'semantic-ui-react'
import {Router} from '../routes';
import carRide from '../ethereum/carRide';

export default class RateDriver extends Component {
  state = { rating: 0,rideID:'',loading:false,errMessage:''}

  async componentWillMount(){
    this.setState({errMessage:''})
  }

  handleChange = e => this.setState({ rating: e.target.value })

  onClickkkk = async()=>{
    const {account} = this.props;
    try{
        this.setState({loading:true,errMessage:''});
        await carRide.methods.rateRide(this.state.rating,this.state.rideID).send({
            from: account
        });
        Router.pushRoute('/');
    }catch(err){
        this.setState({errMessage:err.message})
    }
    this.setState({rating:0,rideID:'',loading:false,errMessage:''});
  }

  render() {
      if(this.props.show){
        const { rating } = this.state
        return (
        <div>
            <Input style={{marginBottom:'0.5rem',marginTop:'0.5rem'}} placeholder='Ride ID' onChange={event =>this.setState({rideID:event.target.value})}/>
            <div>Rating: {rating}</div>
            <input style={{marginTop:'0.3rem'}} type='range' min={1} max={5} value={rating} onChange={this.handleChange} />
            <br />
            <Rating rating={this.state.rating} maxRating={5} />
            <br />
            <Button style={{marginTop:'0.5rem'}} primary onClick={this.onClickkkk} loading={this.state.loading}>
                Send Rating
            </Button>
            <Message error={!!this.state.errMessage} content={this.state.errMessage} />
        </div>
        )
      }else{
          return null;
      }
    
  }
}

import React,{Component} from 'react';
import {Card,Button} from 'semantic-ui-react'


class DriverDetailsAddRide extends Component{

    render(){
        if(this.props.show){
            return(
                <div>
                        <Card fluid  color='violet' style={{width:'80%',marginBottom:'1rem',marginTop:'1rem'}}>
                        <Card.Content>
                            <Card.Header>{this.props.name}</Card.Header>
                            <Card.Meta>{this.props.addr}</Card.Meta>
                            <Card.Description>
                                <h5>Telephone Number:</h5> {this.props.telNr}
                                <hr />
                                <h5>Car Brand:</h5> {this.props.carBrand}
                                <hr />
                                <h5>Licence Number:</h5> {this.props.licenceNr}
                                <hr />
                                <h5>Number of Rides:</h5> {this.props.nrOfRides}
                            </Card.Description>
                        </Card.Content>
                        </Card>
                        </div>
            );
        }else{
            return null;
        }
        
        
    }
}
export default DriverDetailsAddRide;


import React,{Component} from 'react';
import carRide from '../ethereum/carRide';
import {Form,Input,Button,Container,Message} from 'semantic-ui-react';
import web3 from '../ethereum/web3';
import {Router,Link} from '../routes';

class DriverForm extends Component{

    state={
        name:'',
        carBrand:'',
        licenceNr:'',
        telNr:'',
        errors:[],
        errMessage:'',
        loading:false
    }

    validate(name, telNr,licenceNr,carBrand){
        const errors=[];
       

        const regName = new RegExp("^([a-zA-Z -]{2,}\\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)");
        const regLic = new RegExp("((AB)|(AR)|(AG)|B|(BC)|(BH)|(BN)|(BT)|(BV)|(BR)|(BZ)|(CS)|(CL)|(CJ)|(CT)|(CV)|(DB)|(DJ)|(GL)|(GR)|(GJ)|(HR)|(HD)|(IL)|(IS)|(IF)|(MM)|(MH)|(MS)|(NT)|(OT)|(PH)|(SM)|(SJ)|(SB)|(SV)|(TR)|(TM)|(TL)|(VS)|(VL)|(VN))[ ]*[0-9]{2,3}[ ]*[A-Z]{3}");
        const regTel = new RegExp("^(?:(?:(?:00\\s?|\\+)40\\s?|0)(?:7\\d{2}\\s?\\d{3}\\s?\\d{3}|(21|31)\\d{1}\\s?\\d{3}\\s?\\d{3}|((2|3)[3-7]\\d{1})\\s?\\d{3}\\s?\\d{3}|(8|9)0\\d{1}\\s?\\d{3}\\s?\\d{3}))$");
        const regCar = new RegExp("^[a-zA-Z0-9 ]*$");
        const valLic = regLic.test(licenceNr);
        const valTel = regTel.test(telNr);
        const valCar = regCar.test(carBrand);
        const valName = regName.test(name);
        if(!valName || name.length >=30 || name.length<5){
            errors.push('in name');
        }
        if(!valCar || carBrand.length >=15 || carBrand.length<5){
            errors.push('in car brand')
        }
        if(!valLic || licenceNr>=11){
            errors.push('in licence number')
        }
        if(!valTel || telNr >=15){
            errors.push('in telephone number')
        }

        return errors;
    }

    onSubmit = async ()=>{
        event.preventDefault();
      
      
        const {name,carBrand,licenceNr,telNr} = this.state;
        const errors = this.validate(name, telNr,licenceNr, carBrand);
        if (errors.length > 0) {
            this.setState({ errors });
            return;
        }
        this.setState({loading:true,errMessage:'',errors:[]});
        try{
            const accounts = await web3.eth.getAccounts();
            await carRide.methods.registerAsDriver(carBrand,name,licenceNr,telNr).send({
                    from:accounts[0]
            });
            Router.pushRoute('/addRide');
        } catch(err){
            this.setState({errMessage:'Error'});
        }
        
        this.setState({loading:false,name:'',carBrand:'',licenceNr:'',telNr:''});   
    }

   

    render(){
        const { errors } = this.state;
        return(
            <Container style={{width:'70%'}}>
              
                <Form onSubmit={this.onSubmit} error={!!this.state.errMessage}>
                {errors.map(error => (
                        <p style={{color:'red'}} key={error}>Error: {error}</p>
                ))}
                <Form.Field>
                    <label>Name</label>
                    <Input
                        required
                        placeholder='Full Name'
                        value={this.state.name}
                        onChange={event =>this.setState({name:event.target.value})}
                    />
                </Form.Field>
       
                <Form.Field>
                    <label>Car Brand</label>
                    <Input
                        required
                        placeholder='ex: Renault Symbol'
                        value={this.state.carBrand}
                        onChange={event=>this.setState({carBrand:event.target.value})}
                    />
                </Form.Field>
                <Message error 
                    content={this.state.errName} />
                <Form.Field>
                    <label>Licence Number</label>
                    <Input 
                        required
                        placeholder='ex: B 05 BNB'
                        value={this.state.licenceNr}
                        onChange={event => this.setState({licenceNr:event.target.value})}
                    />
                </Form.Field>
          
                <Form.Field>
                    <label>Telephone Number</label>
                    <Input 
                        required
                        placeholder='ex: 00 40 218 032 329 /0722 000 000'
                        value={this.state.telNr}
                        onChange={event=> this.setState({telNr:event.target.value})}
                    />
                </Form.Field>
               
                <Button primary loading={this.state.loading}>Register</Button>
                <Message error 
                    content={this.state.errMessage} />
            </Form>
            </Container>
            
        );
    }
}

export default DriverForm;
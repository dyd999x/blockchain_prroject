import React,{Component} from 'react';
import {Table,Button,Rating} from 'semantic-ui-react';
import {Link} from '../routes';


class RideRow extends Component{
  

    render(){
        const {Row,Cell} = Table;
        const {id,ride,rating} = this.props;
        const size='small'
        return(
            <Row textAlign='center' disabled={ride.completed || ride.seatsAvailable==0 }>
                <Cell  >{id}</Cell>
                <Cell  >{ride.startLocation}</Cell>
                <Cell  >{ride.destination}</Cell>
                <Cell  >{ride.seatsAvailable}</Cell>
                <Cell  >{ride.rideDate}</Cell>
                <Cell>
                    <Link route={`/selectRide/${this.props.account}/${id}/${ride.seatsAvailable}`} >
                        <a>
                        <Button 
                        basic 
                        color='violet' 
                        content='Select Ride' 
                        disabled={ride.completed || ride.seatsAvailable==0}
                        />
                        </a>
                   
                    </Link>
                </Cell>
            </Row>
            
        )
    }
}

export default RideRow;
import React,{Component} from 'react';
import {Table,Segment,Header,Icon,Container} from 'semantic-ui-react';
import RideRow from './RideRow';
import Layout from './Layout';

class RenderRides extends Component{

    renderRows(){
        return this.props.rides.map((ride,index)=>{
            return(
                <RideRow
                    key={index}
                    id={ride.id}
                    ride={ride}
                    rating={this.props.driversRatings[index]}
                    account={this.props.account}
                />
            );
        });
    }

    render(){
        if(this.props.rides.length > 0){
            const {Header,Row,HeaderCell,Body} = Table;
            return(
            <Table stackable striped color="violet" style={{marginBottom:'2rem',marginTop:'2rem'}}>
                <Header>
                    <Row textAlign='center'>
                        <HeaderCell >Ride ID</HeaderCell>
                        <HeaderCell >From</HeaderCell>
                        <HeaderCell>Destination</HeaderCell>
                        <HeaderCell >Seats Available</HeaderCell>
                        <HeaderCell>Date</HeaderCell>
                        <HeaderCell>Share!</HeaderCell>
                    </Row>
                </Header>
                <Body>
                    {this.renderRows()}
                </Body>
            </Table>
            )
        }else{
            return(
                <Container fluid>
                     <Segment placeholder>
                        <Header icon>
                            <Icon name='file outline' />
                            There are no rides yet
                        </Header>
                    </Segment>
                </Container>
                   
               
            )
        }

        
        
    }
}
export default RenderRides;
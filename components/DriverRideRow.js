import React,{Component} from 'react';
import {Table,Button} from 'semantic-ui-react';
import carRide from '../ethereum/carRide';
import {Router,Link} from '../routes';


class DriverRideRow extends Component{
    state={
        mess:''
    }

    onClikkk = async()=>{
        const addr = this.props.ride.driver;
        try{
            await carRide.methods.rideCompleted(this.props.id).send({
                from: addr
            });
            Router.pushRoute('/');
        }catch(err){
            this.setState({mess:'Something went wrong!'});
        }
        
    }

    render(){
        const {Row,Cell} = Table;
        const {id,ride,addr} = this.props;
        if(ride.driver == addr){
            let status='';
            let cl=''
            if(ride.completed){
                status='Completed';
                cl='red'
            }
            else{
                status ='Finish';
                cl='green'
            }
               
            return(
                <Row textAlign='center'  negative={!!this.state.mess}>
                    <Cell disabled={ride.completed}>{id}</Cell>
                    <Cell disabled={ride.completed}>{ride.startLocation}</Cell>
                    <Cell disabled={ride.completed}>{ride.destination}</Cell>
                    <Cell disabled={ride.completed}>{ride.rideDate}</Cell>
                    <Cell>
                        <Button 
                            basic 
                            color={`${cl}`} 
                            content={`${status}`}
                            disabled={ride.completed}
                            onClick={this.onClikkk}
                            />
                    </Cell>
                    <Cell>
                        <Link route={`/passDetails/${this.props.id}`}>
                            <a>
                                <Button
                                basic
                                color='violet'
                                content='See Details'
                                
                            />
                            </a>

                        </Link>
                        
                    </Cell>
                </Row>
            )
        }else{
            return null;
        }
        
    }
}

export default DriverRideRow;
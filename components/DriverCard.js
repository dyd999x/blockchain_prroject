import React,{Component} from 'react';
import {Card,Button} from 'semantic-ui-react'
import DriverRating from './DriverRating';
import DriverRides from './DriverRides';
import {Router} from '../routes';

class DriverCard extends Component{

    state={
        showRides:false,
        myRides:[]
    }

    onClickRides = async () =>{
        if(!this.state.showRides){
        
            this.setState({showRides:true});
        }

            
        else{
            this.setState({showRides:false});
        }
    }

    render(){
        if(this.props.show){
            const size='small'
            return(
                <div>
                        <Card  raised style={{width:'60%',marginTop:'3rem',marginBottom:'3rem'}} color='violet'>
                        <Card.Content>
                            <Card.Header>{this.props.name}</Card.Header>
                            <Card.Meta>{this.props.addr}</Card.Meta>
                            <Card.Description>
                                <h5>Telephone Number:</h5> {this.props.telNr}
                                <hr />
                                <h5>Car Brand:</h5> {this.props.carBrand}
                                <hr />
                                <h5>Licence Number:</h5> {this.props.licenceNr}
                                <hr />
                                <h5>Number of Rides:</h5> {this.props.nrOfRides}
                                <hr />
                                <h5>Rating:</h5>
                                <h6>{this.props.nrOfVotes} Votes</h6>
                                <DriverRating rating={this.props.rating} size={size}/>
                            </Card.Description>
                        </Card.Content>
                        <Card.Content extra>
                            <Button basic color='blue' content='Show My Rides' onClick={this.onClickRides}/>
                            <Button basic color='blue' content='Add Ride' onClick={()=>Router.pushRoute('/addRide')}/>
                        </Card.Content>
                        </Card>
                  
                        <DriverRides rides={this.props.rides} show={this.state.showRides}
                         addr={this.props.addr}/>
                        </div>
            );
        } else {
            return(<div></div>);
        }
        
    }
}

export default DriverCard;


import React from 'react';
import {Header,Segment} from 'semantic-ui-react';
import {Link} from '../routes';

export default() =>{
    return(
        <Segment clearing inverted color='violet'>
            <Header as='h2' style={{margin:'0.5rem'}} color='olive'>
                <Link route="/" >
                    <a className="item" style={{color:'white'}}>
                        Ride Sharing
                    </a>
                </Link>
            </Header>
        </Segment>
    )
}
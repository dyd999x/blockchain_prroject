import React,{Component} from 'react';
import carRide from '../ethereum/carRide';
import {Form,Input,Button,Container,Message} from 'semantic-ui-react';
import web3 from '../ethereum/web3';
import {Router} from '../routes';

class RideForm extends Component{

    state={
        from:'',
        destination:'',
        ridedate:'',
        seats:'',
        errMessage:'',
        errors:[],
        loading:false
    }

    validate(from,destination,rideDate,seats){
        const errors =[];
        const reg = new RegExp("^[a-zA-Z0-9 ]*[a-zA-Z0-9., ]*$");
        const regDate = new RegExp("^(0[1-9]|1\\d|2\\d|3[01])\\/(0[1-9]|1[0-2])\\/(20)\\d{2}$");
        const fromT = reg.test(from);
        const destT = reg.test(destination);
        const dateT = regDate.test(rideDate);

        if(!fromT || from.length >=50 || from.length <=10){
            errors.push('in from field');
        }
        if(!destT || destination.length>=50 || from.length <=10){
            errors.push('in destination field');
        }
        if(!dateT || rideDate.length >10){
            errors.push('in ride date');
        }
        let bec=0;
        for(var i=1;i<=7;i++){
            if(seats == i){
                bec=1;
                break;
            }  
        }
        if(bec == 0){
            errors.push('in seats: min=1 max=7');
        }
        return errors;
    }

    onSubmit = async ()=>{
        event.preventDefault();

        const {from,destination,ridedate,seats} = this.state;

       
        const errors = this.validate(from, destination,ridedate,seats);
        if (errors.length > 0) {
           
            this.setState({ errors });
            return;
        }
        this.setState({loading:true,errMessage:'',errors:[]});
        
        try{
            const accounts = await web3.eth.getAccounts();
            const rideID = await carRide.methods.addRide(from,destination,ridedate,seats).send({
                    from:accounts[0]
            });
            Router.pushRoute('/');
        } catch(err){
                this.setState({errMessage:'Error'});
        }
        
        this.setState({loading:false,from:'',destination:'',rideDate:'',seats:''});   
    }
    render(){
        const { errors } = this.state;
        return(
            <Container style={{width:'70%'}}>
                <Form onSubmit={this.onSubmit} error={!!this.state.errMessage}>
                {errors.map(error => (
                        <p style={{color:'red'}} key={error}>Error: {error}</p>
                ))}
                <Form.Field>
                    <label>From</label>
                    <Input
                        required
                        placeholder='ex: Sector5, Bucuresti'
                        value={this.state.from}
                        onChange={event =>this.setState({from:event.target.value})}
                    />
                </Form.Field>
       
                <Form.Field>
                    <label>Destination</label>
                    <Input
                        required
                        placeholder='ex: Timisoara'
                        value={this.state.destination}
                        onChange={event=>this.setState({destination:event.target.value})}
                    />
                </Form.Field>
          
                <Form.Field>
                    <label>Date</label>
                    <Input 
                        required
                        placeholder='dd/mm/yyyy ex: 07/08/2019'
                        value={this.state.ridedate}
                        onChange={event => this.setState({ridedate:event.target.value})}
                    />
                </Form.Field>
          
                <Form.Field>
                    <label>Available Seats</label>
                    <Input 
                        required
                        type='number'
                        placeholder='max=7'
                        value={this.state.seats}
                        onChange={event=> this.setState({seats:event.target.value})}
                    />
                </Form.Field>
               
                <Button primary loading={this.state.loading}>Add</Button>
                <Message error content={this.state.errMessage} />
            </Form>
            </Container>
            
        );
    }
}

export default RideForm;
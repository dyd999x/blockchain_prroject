import React,{Component} from 'react';
import carRide from '../ethereum/carRide';
import {Form,Input,Button,Container,Message,Segment} from 'semantic-ui-react';
import web3 from '../ethereum/web3';
import {Router} from '../routes';
import RideSelected from './RideSelected';
import DriverDetails from './DriverDetails';

class SelectRide extends Component{

    state={
        name:'',
        telNr:'',
        seats:'',
        seatsAv:'',
        errMessage:'',
        driverTelNr:'',
        carBrand:'',
        licenceNr:'',
        nrOfRides:'',
        rating:'',
        nrOfVotes:'',
        driverName:'',
        errors:[],
        loading:false
    }

    validate(name,telNr,seats){
        const errors =[];
        const regTel = new RegExp("^(?:(?:(?:00\\s?|\\+)40\\s?|0)(?:7\\d{2}\\s?\\d{3}\\s?\\d{3}|(21|31)\\d{1}\\s?\\d{3}\\s?\\d{3}|((2|3)[3-7]\\d{1})\\s?\\d{3}\\s?\\d{3}|(8|9)0\\d{1}\\s?\\d{3}\\s?\\d{3}))$");
        const regName = new RegExp("^([a-zA-Z -]{2,}\\s[a-zA-z]{1,}'?-?[a-zA-Z]{2,}\\s?([a-zA-Z]{1,})?)");
        const valName = regName.test(name);
        const valTel = regTel.test(telNr);

        if(!valName || name.length >=30){
            errors.push('in name');
        }
        if(!valTel || telNr >=15){
            errors.push('in telephone number')
        }
        if(seats>this.state.seatsAv || seats <1){
            errors.push('in seats: min=1 max= seats available');
        }
        return errors;
    }

    onSubmit = async ()=>{
        event.preventDefault();
      
        const {name,telNr,seats} = this.state;

        const errors = this.validate(name, telNr,seats);
        if (errors.length > 0) {
            this.setState({ errors });
            return;
        }
        const {account,rideID} = this.props;
      
        this.setState({loading:true,errMessage:'',rideID:rideID,errors:[]});
        try{
            await carRide.methods.selectRide(rideID,seats,name,telNr).send({
                    from:account
            });
            Router.pushRoute('/');
        } catch(err){
            this.setState({errMessage:'Error'});
        }
        
        this.setState({loading:false,name:'',telNr:'',seats:'',errors:[]});   
    }

    async componentWillMount(){
        const {account,rideID} = this.props;
        const ride = await carRide.methods.rides(rideID).call();
        const driver = await ride.driver;
        const details = await carRide.methods.drivers(driver).call();
        this.setState({
            seatsAv:ride.seatsAvailable,
            driverName:details.name,
            driverTelNr:details.telNr,
            nrOfRides:details.nrOfRides,
            licenceNr:details.licenseNr,
            carBrand:details.carBrand
        });
        const ratings = await carRide.methods.driverRating(driver).call();
        let j = 5;
        let rating =0;
        let nr=0;
        for(var i=0;i<5;i++){
            rating+=parseInt(ratings[i])*j;
            nr+=parseInt(ratings[i]);
            j--;
        }
        if(nr!=0)rating /= nr;
        else rating=0;
        this.setState({rating:rating,nrOfVotes:nr});
    }
    render(){
        const { errors } = this.state;
        return(
            <div>
                <Container style={{marginBottom:'2rem',marginTop:'1rem'}} fluid>
                    <RideSelected rideID={this.props.rideID} />
                </Container>
                <Container style={{marginBottom:'2rem'}} fluid>
                    <Segment.Group horizontal>
                        <Segment padded>
                            <DriverDetails 
                            name={this.state.driverName} 
                            telNr={this.state.driverTelNr}
                            carBrand={this.state.carBrand}
                            licenceNr={this.state.licenceNr}
                            nrOfRides={this.state.nrOfRides}
                            rating={this.state.rating}
                            nrOfVotes={this.state.nrOfVotes}
                            />
                        </Segment>
                        <Segment padded secondary>
                            <Form onSubmit={this.onSubmit} error={!!this.state.errMessage} >
                            {errors.map(error => (
                            <p style={{color:'red'}} key={error}>Error: {error}</p>
                                ))}
                                <Form.Field>
                                    <label>Name</label>
                                    <Input
                                        required
                                        placeholder='Full Name'
                                        value={this.state.name}
                                        onChange={event =>this.setState({name:event.target.value})}
                                    />
                                </Form.Field>

                                <Form.Field>
                                    <label>Number Of Seats</label>
                                    <Input
                                        required
                                        placeholder={`max ${this.props.seatsAv}`}
                                        value={this.state.seats}
                                        onChange={event=>this.setState({seats:event.target.value})}
                                    />
                                </Form.Field>

                                <Form.Field>
                                    <label>Telephone Number</label>
                                    <Input 
                                        required
                                        placeholder='ex: 00 40 218 032 329 /0722 000 000'
                                        value={this.state.telNr}
                                        onChange={event=> this.setState({telNr:event.target.value})}
                                    />
                                </Form.Field>
                            
                                <Button primary loading={this.state.loading}>Select Ride</Button>
                                <Message error content={this.state.errMessage} />
                            </Form>
                        </Segment>
                    </Segment.Group>
                   
                    
                </Container>
            </div>
            
            
        );
    }
}

export default SelectRide;
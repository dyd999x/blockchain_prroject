import React,{Component} from 'react';
import {Card,Image} from 'semantic-ui-react'

class PassDetails extends Component{

    render(){
        {
            const {pass} = this.props;
            const name= pass.name;
            const telNr=pass.telNr;
            return(
                        <Card  raised style={{width:'35%',marginTop:'3rem',marginBottom:'3rem'}} color='violet'>
                        <Card.Content>
                            <Image floated='right' size='mini' src='https://react.semantic-ui.com/images/avatar/large/steve.jpg' />
                            <Card.Header>{name}</Card.Header>
                            <Card.Description>
                                <h5>Telephone Number:</h5> {telNr}
                                <hr />
                            </Card.Description>
                        </Card.Content>
                       
                        </Card>

            );
        }
        
    }
}

export default PassDetails;


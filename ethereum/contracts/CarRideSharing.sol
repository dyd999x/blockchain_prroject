pragma solidity ^0.4.24;

contract CarRideSharing{
    
    address public creator;
    uint public rideID;
    
    struct Driver{
        address addr;
        string name;
        string carBrand;
        string licenseNr;
        string telNr;
        uint nrOfRides;
    }
    
    struct Rating{
        uint excelent;
        uint veryGood;
        uint good;
        uint bad;
        uint veryBad;
    }
    
    struct Ride{
        uint id;
        address driver;
        string startLocation;
        string destination;
        string rideDate;
        uint seatsAvailable;
        address[] passengers;
        bool completed;
    }
    
    struct Passenger{
        address addr;
        string name;
        string telNr;
    }
    
    constructor() public{
        creator =msg.sender;
        rideID=0;
    }
    
    mapping(address => Driver) public drivers;
    mapping(uint => Ride) public rides;
    mapping(address => Passenger) public pass;
    mapping(address => Rating) public driverRating;
    
    event newDriver(address DriverAddress);
    event newRide(uint RideID);
    event newPassengers(uint RideID,address passengers);
    
    function registerAsDriver(string _carBrand,string _name, string _licenseNr,string _telNr) public {
        require(drivers[msg.sender].addr == 0);
        drivers[msg.sender] = Driver(msg.sender,_name,_carBrand,_licenseNr,_telNr,0);
        Rating memory r = Rating(0,0,0,0,0);
        driverRating[msg.sender] = r;
        emit newDriver(msg.sender);
    }
    
    function addRide(string _startLocation,string _destination,string _rideDate,uint _seatsAvailable) public returns(uint RideID){
        require(drivers[msg.sender].addr == msg.sender && _seatsAvailable <=7);
        rides[rideID] = Ride(rideID,msg.sender,_startLocation,_destination,_rideDate,_seatsAvailable, new address[](0),false);
        emit newRide(rideID);
        drivers[msg.sender].nrOfRides++;
        RideID = rideID;
        rideID++;
    }
    
    function selectRide(uint _rideID,uint _seats,string _name, string _telNr) public{
        require(rides[_rideID].seatsAvailable >= _seats);
        Passenger memory p = Passenger(msg.sender,_name,_telNr);
        rides[_rideID].seatsAvailable -= _seats;
        pass[msg.sender] = p;
        rides[_rideID].passengers.push(msg.sender);
        emit newPassengers(_rideID,msg.sender);
    }
    
    function rateRide(uint rating, uint _rideID) public{
        require(rating >=1 && rating <=5 && rides[_rideID].completed == true);
        address driver = rides[_rideID].driver;
        bool bec = false;
        for(uint i=0;i<rides[_rideID].passengers.length;i++){
            if(rides[_rideID].passengers[i] == msg.sender){
                bec = true;
                break;
            }
        }
        require(bec == true);
        if(rating == 1)driverRating[driver].veryBad++;
        if(rating == 2)driverRating[driver].bad++;
        if(rating == 3)driverRating[driver].good++;
        if(rating == 4)driverRating[driver].veryGood++;
        if(rating == 5)driverRating[driver].excelent++;
    }
    
    function rideCompleted(uint _rideID) public{
        require(msg.sender == rides[_rideID].driver && rides[_rideID].completed ==false);
        rides[_rideID].completed = true;
    }

    
    function getRidePassengers(uint _rideID) public view returns(address[] Pass){
        Pass = rides[_rideID].passengers;
    }
    
    
}
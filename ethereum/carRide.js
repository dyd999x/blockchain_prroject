import web3 from './web3';
import CarRide from './build/CarRideSharing.json';

const instance = new web3.eth.Contract(
    JSON.parse(CarRide.interface),
    '0x448c86b7AE9E79329c4349dF95f782131E21F007'
   
);

export default instance;